import java.io.*;
import java.util.*;

public class Manip {

	/** *********************************************** */
	/* Cette mthode a pour but de convertir une */
	/* chaine de caractres en tableau de caractres */
	/*                                                */
	/** *********************************************** */
	public static String[] CutInStrings(String chaine) {

		int nbr_carac = 0;

		StringTokenizer tok = new StringTokenizer(chaine, " ");

		try {
			nbr_carac = tok.countTokens();
			System.out.println("Ce symbol est compos de " + nbr_carac
					+ " parties");
		} catch (Exception e) {
			System.exit(0);
		}

		String[] tableau = new String[nbr_carac];

		for (int i = 0; i < nbr_carac; i++) {
			tableau[i] = tok.nextToken();
			System.out.print(tableau[i] + " ");
		}
		System.out.println();

		return (tableau);
	}

	public static String[] ReadFile(String NomFich) {
		int i;
		int NbrRules;

		String[] SymbolTab = null;
		String ligne_lue1 = null;

		Vector TempVector = new Vector();

		FileReader fr = null;
		BufferedReader br = null;

		try {
			fr = new FileReader(NomFich);
			br = new BufferedReader(fr);
		} catch (Exception e) {
			System.out.println("Fichier pas trouv dans Launch.java");
		}

		NbrRules = 0;
		// tant qu'on a pas atteint la fin du fichier
		// on rcupre la rfrence  une Forme
		do {
			try {
				ligne_lue1 = br.readLine();

				/*
				 * int i = 0 ; while( SymbolTab[i] != null ) {
				 * System.out.println( SymbolTab[i] ) ; i++ ; }
				 */
			} catch (Exception e) {
				System.out.println("Impossible de lire dans le fichier !");
			}
			if (ligne_lue1 != null) {
				TempVector.add(new String(ligne_lue1));
				// System.out.println( SymbolTab[i] ) ;
				NbrRules++;
			}

		} while (ligne_lue1 != null);

		SymbolTab = new String[NbrRules];
		for (i = 0; i < NbrRules; i++) {
			SymbolTab[i] = (String) TempVector.elementAt(i);
		}

		return SymbolTab;
	}

}
