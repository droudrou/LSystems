
import javax.swing.*;
import java.awt.*;
//import java.io.*;

/**
 * Cette classe dfinit les champs suivant de l'interface graphique :
 * <ul>
 * <li> le champ de texte de l'axiome du LSystem
 * <li> le champ de texte de la profondeur du LSystem
 * <li> le champ de texte de l'angle de rotation de la "tortue"
 * <li> le champ de texte de la longueur d'un trait dessin par la tortue
 * <li> le champ de texte de l'abcisse de rfrence
 * <li> le champ de texte de l'ordonne de rfrence
 * </ul>
 * <p>
 * Cette classe contient donc essentiellement des mthodes Get() qui seront appeles 
 * lorsqu'on demendra l'affichage du LSystem.
 * 
 * @author Mathieu Drouin
 * @version 0.9
 *
 */
public class AxiomAndSettings extends JPanel /* implements ActionListener */
{	
	private static final long serialVersionUID = 00001000L;

	private JTextField JTxt_Axiom;

	private JLabel JLab_Axiom;

	private JTextField JTxt_Profondeur;

	private JLabel JLab_Profondeur;

	private JTextField JTxt_Angle;

	private JLabel JLab_Angle;

	private JTextField JTxt_InitialAngle;

	private JLabel JLab_InitialAngle;
	
	private JTextField JTxt_LineLength;

	private JLabel JLab_LineLength;

	private JTextField JTxt_PosX;

	private JLabel JLab_PosX;

	private JTextField JTxt_PosY;

	private JLabel JLab_PosY;

	/**
	 * Le constructeur de la classe AxiomAndSettings, il initialise les diffrents champs
	 * de la classe et les met en forme.
	 * 
	 */
	public AxiomAndSettings() {
		JLab_Axiom = new JLabel("Axiom");
		JTxt_Axiom = new JTextField(10);
		JTxt_Axiom.setMaximumSize(new Dimension(300, 20));

		JLab_Profondeur = new JLabel("Iteration");
		JTxt_Profondeur = new JTextField(10);
		JTxt_Profondeur.setMaximumSize(new Dimension(50, 20));

		JLab_Angle = new JLabel("Angle");
		JTxt_Angle = new JTextField(10);
		JTxt_Angle.setMaximumSize(new Dimension(100, 20));
		
		JLab_InitialAngle = new JLabel("Initial Angle");
		JTxt_InitialAngle = new JTextField(10);
		JTxt_InitialAngle.setMaximumSize(new Dimension(100, 20));

		JLab_LineLength = new JLabel("Line length");
		JTxt_LineLength = new JTextField(10);
		JTxt_LineLength.setMaximumSize(new Dimension(50, 20));

		JLab_PosX = new JLabel("offset x =");
		JTxt_PosX = new JTextField(10);
		JTxt_PosX.setMaximumSize(new Dimension(100, 20));

		JLab_PosY = new JLabel("offset y =");
		JTxt_PosY = new JTextField(10);
		JTxt_PosY.setMaximumSize(new Dimension(100, 20));

		this.setBorder(BorderFactory.createTitledBorder(BorderFactory
				.createEtchedBorder(), "Axiom and Settings"));
		// this.setLayout( new BoxLayout( this, BoxLayout.Y_AXIS) ) ;

		Box box1 = Box.createVerticalBox();
		this.setMaximumSize(new Dimension(250, 350));

		box1.add(JLab_Axiom);
		box1.add(JTxt_Axiom);
		box1.add(JLab_Profondeur);
		box1.add(JTxt_Profondeur);
		box1.add(JLab_Angle);
		box1.add(JTxt_Angle);
		box1.add(JLab_InitialAngle);
		box1.add(JTxt_InitialAngle);
		box1.add(JLab_LineLength);
		box1.add(JTxt_LineLength);
		box1.add(JLab_PosX);
		box1.add(JTxt_PosX);
		box1.add(JLab_PosY);
		box1.add(JTxt_PosY);

		this.add(box1);
	}

	/**
	 * Renvoie la chane correspondant  l'axiome du LSystem.
	 * 
	 * @return Une chane vide si JTxt_Axiom est vide
	 */
	public String GetAxiom() {
		return (new String(JTxt_Axiom.getText()));
	}

	/**
	 * Renvoie la profondeur du LSystem  construire.
	 * 
	 * @return Si JTxt_Profondeur est invalide, une profondeur de 1 est 
	 * renvoye par dfaut
	 * @throws NumberFormatException On doit rcuprer un entier
	 */
	public int GetProfondeur(){
		int prof;
		String str_temp;

		try {
			str_temp = JTxt_Profondeur.getText();
			prof = Integer.parseInt(str_temp);
		}
		catch(NumberFormatException excep) {
			JTxt_Profondeur.setText("1");
			str_temp = JTxt_Profondeur.getText();
			prof = Integer.parseInt(str_temp);
		}
		
		if (prof>20) {
			prof = 20;			
		}

		return prof;
	}

	/**
	 * Renvoie l'angle de rotaton associ  un ordre de rotation.
	 * 
	 * @return Si JTxt_Angle est invalide, un angle de 30 est choisi par dfaut
	 * @throws NumberFormatException On doit rcuprer un Float
	 */
	public float GetAngle() {
		float angle;
		String str_temp;

		try {
			str_temp = JTxt_Angle.getText();
			angle = Float.parseFloat(str_temp);
		}
		catch(NumberFormatException excep){
			JTxt_Angle.setText("30");
			str_temp = JTxt_Angle.getText();
			angle = Float.parseFloat(str_temp);
		}		

		return angle;
	}
	
	/**
	 * Renvoie l'angle initial d'orientation du dessin.
	 * 
	 * @return Si JTxt_InitialAngle est invalide, un angle de 0 est choisi par dfaut
	 * @throws NumberFormatException On doit rcuprer un Float
	 */
	public float GetInitialAngle() {
		float InitialAngle;
		String str_temp;

		try {
			str_temp = JTxt_InitialAngle.getText();
			InitialAngle = Float.parseFloat(str_temp);
		}
		catch(NumberFormatException excep){
			JTxt_InitialAngle.setText("180");
			str_temp = JTxt_InitialAngle.getText();
			InitialAngle = Float.parseFloat(str_temp);
		}		

		return InitialAngle;
	}

	/**
	 * Renvoie la longueur d'un trait associe  un ordre de dessin.
	 * 
	 * @return Si JTxt_LineLength est invalide, une longueur par dfaut de 10 est choisie
	 * @throws NumberFormatException On doit rcuprer un Float
	 */
	public float GetLength() {
		float length;
		String str_temp;

		try {
			str_temp = JTxt_LineLength.getText();
			length = Float.parseFloat(str_temp);
		}
		catch(NumberFormatException excep) {
			JTxt_LineLength.setText("10");
			str_temp = JTxt_LineLength.getText();
			length = Float.parseFloat(str_temp);
		}

		if(length>100){
			length = 10;
		}

		return length;
	}

	/**
	 * Renvoie l'abcisse o le dessin va tre dessin.
	 * 
	 * @return Si JTxt_PosX est invalide, on place l'OffsetX  400
	 * @throws NumberFormatException On doit rcuprer un Float
	 */
	public float GetPosX() {
		float OffsetX;
		String str_temp;

		try {
			str_temp = JTxt_PosX.getText();
			OffsetX = Float.parseFloat(str_temp);
		}
		catch(NumberFormatException excep){
			JTxt_PosX.setText("400");
			str_temp = JTxt_PosX.getText();
			OffsetX = Float.parseFloat(str_temp);
		}

		if(OffsetX>2000 || OffsetX<-1000){			
			OffsetX = 400;
		}

		return OffsetX;
	}

	/**
	 * Renvoie l'ordonne o le dessin va tre dessin.
	 * 
	 * @return Si JTxt_PosY est invalide, on place l'OffsetY  50
	 * @throws NumberFormatException On doit rcuprer un Float
	 */
	public float GetPosY() {
		float OffsetY;
		String str_temp;

		try {
			str_temp = JTxt_PosY.getText();
			OffsetY = Float.parseFloat(str_temp);
		}
		catch(NumberFormatException excep){
			JTxt_PosY.setText("500");
			str_temp = JTxt_PosY.getText();
			OffsetY = Float.parseFloat(str_temp);
		}

		if(OffsetY>2000 || OffsetY<-1000){			
			OffsetY = 500;
		}

		return OffsetY;
	}
	
	
	public void SetAxiom(String Axiom) {		
		JTxt_Axiom.setText(Axiom);
	}
	
	public void SetAngle(String Angle) {
		JTxt_Angle.setText(Angle);
	}	
	
	public void SetProfondeur(String Profondeur) {
		JTxt_Profondeur.setText(Profondeur);
	}
	
	public void SetLength(String Length) {
		JTxt_LineLength.setText(Length);
	}	

}
