import java.io.*;

public class Clavier {

	public static String lireString() {
		String ligne_lue = null;
		try {
			InputStreamReader lecteur = new InputStreamReader(System.in);
			BufferedReader entree = new BufferedReader(lecteur);
			ligne_lue = entree.readLine();
		}

		catch (IOException err) {
			System.exit(0);
		}

		return ligne_lue;
	}

	public static float lireFloat() {
		float x = 0;
		try {
			String ligne_lue = lireString();
			Float fwrap = new Float(ligne_lue);
			x = fwrap.floatValue();
		}

		catch (NumberFormatException err) {
			System.out.println("***** Erreur de donne ****");
			System.exit(0);
		}

		return x;
	}

	public static double lireDouble() {
		double x = 0;
		try {
			String ligne_lue = lireString();
			Double dwrap = new Double(ligne_lue);
			x = dwrap.doubleValue();
		}

		catch (NumberFormatException err) {
			System.out.println("***** Erreur de donne ****");
			System.exit(0);
		}

		return x;
	}

	public static int lireInt() {
		int x = 0;
		try {
			String ligne_lue = lireString();
			x = Integer.parseInt(ligne_lue);
		}

		catch (NumberFormatException err) {
			System.out.println("***** Erreur de donne ****");
			System.exit(0);
		}

		return x;
	}

}
