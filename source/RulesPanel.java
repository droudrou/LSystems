
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.table.*;

//import java.io.*;

/**
 * Cette classe permet la saisie des rgles, et leur rcupration en vue d'tre traites par les
 * mthodes de la classe Engine.
 * <p>
 * L'attribut tableau de type JTable permet de saisir les rgles, sous forme de chanes 
 * de caractres brutes.
 * Elles devront ensuite tre traites, pour en extraire les rgles et symboles gnrateurs 
 * associs  ces rgles (cf classe Engine).
 * <p>
 * La construction de la JTable se fait via la mthode InitTable().
 * La rcupration des rgles se fait via la mthode GetRules(). 
 * <p>
 * Ce panneau contient deux boutons :
 * <ul>
 * <li> But_AddLine : pour ajouter une ligne  l'attribut tableau.
 * <li> But_DeleteLine : pour retirer une ligne  l'attribut tableau.
 * </ul>
 * 
 * 
 * @author Mathieu Drouin
 * @version 0.9
 */
public class RulesPanel extends JPanel implements ActionListener {

	private static final long serialVersionUID = 00001000L;

	private JScrollPane scrollpane;

	private DefaultTableModel dftable;

	private JTable tableau;

	private JButton But_AddLine;

	private JButton But_DeleteLine;
	
	private JButton But_RemoveEmptyLines;
	
	private JButton But_DeleteAllLines;

	private int nbr_col;

	/**
	 * Le constructeur, il prend en charge l'initialisation des composants graphiques ;
	 * ainsi que leur mise en forme graphique.
	 * <p>
	 * On notera en particulier le composant scrollpane, qui permet de prendre en charge
	 * un grand nombre de rgles si ncessaire, via une barre de dfilement.
	 * <p>
	 * L'initialisation de la JTable, qui permet de saisir les rgles, se fait via la 
	 * mthode ddie InitTable().
	 * 
	 */
	public RulesPanel() {
		But_AddLine = new JButton("Add Line");
		But_DeleteLine = new JButton("Delete Line");
		But_RemoveEmptyLines = new JButton("Remove Empty Lines");
		But_DeleteAllLines = new JButton("Delete All Lines");

		But_AddLine.addActionListener(this);
		But_DeleteLine.addActionListener(this);
		But_RemoveEmptyLines.addActionListener(this);
		But_DeleteAllLines.addActionListener(this);

		InitTable();
		scrollpane = new JScrollPane(tableau,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		scrollpane.setPreferredSize(new Dimension(240, 150));

		this.setBorder(BorderFactory.createTitledBorder(BorderFactory
				.createEtchedBorder(), "Rules"));

		Box box1 = Box.createVerticalBox();
		Box box11 = Box.createHorizontalBox();
		Box box12 = Box.createHorizontalBox();
		Box box13 = Box.createHorizontalBox();
		
		this.setMaximumSize(new Dimension(250, 550));

		box1.setMaximumSize(new Dimension(250, 550));
		box11.setMaximumSize(new Dimension(200, 100));
		box12.setMaximumSize(new Dimension(200, 100));
		box13.setMaximumSize(new Dimension(200, 100));

		box1.add(scrollpane);

		box11.add(But_AddLine);
		box11.add(But_DeleteLine);
		box12.add(But_RemoveEmptyLines);
		box13.add(But_DeleteAllLines);

		box1.add(Box.createVerticalStrut(20));

		box1.add(box11);
		box1.add(box12);
		box1.add(box13);

		this.add(box1);
		this.repaint();
	}

	/**
	 * Cette mthode permet d'initialiser le champ tableau de type JTable.
	 * On met en place un gestionnaire par dfaut de la JTable, et on la remplit 
	 * avec 5 lignes vides.
	 * 
	 */
	public void InitTable() {
		dftable = new DefaultTableModel();
		tableau = new JTable(dftable);
		TableColumn column = null;

		tableau.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

		int nbr_ligne = 5;
		nbr_col = 1;

		// lecture du fichier slectionn
		// int nbrcol, numline = 1 ;
		Object line[] = new Object[nbr_col];

		dftable.addColumn("Rules");
		column = tableau.getColumnModel().getColumn(0);
		column.setPreferredWidth(100);

		// dftable.addColumn("Replace with");
		// column = tableau.getColumnModel().getColumn(1);
		// column.setPreferredWidth(100);

		for (int i = 0; i < nbr_col; i++) {
			line[i] = "";
		}

		for (int i = 0; i < nbr_ligne; i++) {
			dftable.addRow(line);
		}

	}

	/**
	 *  
	 * @return Renvoie une rfrence au tableau de rgles
	 */
	public JTable GetTableau() {
		return tableau;
	}

	/**
	 * @return Renvoie une rfrence au modle de la JTable
	 */
	public DefaultTableModel Getdftable() {
		return dftable;
	}

	/**
	 * Cette mthode permet d'extraire et de rcuprer les rgles du LSystem.
	 * <p>
	 * Notons qu'un premier tri trivial est effectu, on ne prend pas en compte les 
	 * cases vides de la JTable. 
	 * 
	 * 
	 * @return Un tableau de chanes, qui contient les rgles du LSystem
	 */
	public String[] GetRules() {
		int k = 0;
		int NbrLine;
		String[] RulesTab = null;
		String[] temp_RulesTab = null;

		String tempString;

		// on compte le nombre de ligne du tableau
		NbrLine = tableau.getRowCount();
		temp_RulesTab = new String[NbrLine];

		// on scanne chaque ligne
		for (int i = 0; i < NbrLine; i++) {
			tempString = (String) tableau.getValueAt(i, 0);
			if (tempString != null && !tempString.equals("")) {
				temp_RulesTab[k] = tempString;
				k++;
			} else {
				System.out.println("Rgle fictive repre dans GetRules() ");
			}

		}

		RulesTab = new String[k];
		for (int i = 0; i < k; i++) {
			RulesTab[i] = temp_RulesTab[i];
		}

		return RulesTab;
	}
	
	
	public void SetRules(String[] Rules, int NbrRules){
		
		String tempString;		
		
		int nbrLineDif;

		if (tableau.getRowCount() < NbrRules) {
			nbrLineDif = NbrRules - tableau.getRowCount();

			for (int i = 0; i < nbrLineDif; i++) {
				dftable.addRow(new Object[1]);
			}
		}

		try {
			for(int i=0 ; i < NbrRules ; i++) {
				
				tempString = Rules[i];

				if (tempString == null) {
					System.out.println("Ligne vide dans SetRules()");
					Rules[i] = null;
					break;
				} else {
					tableau.setValueAt((Object)tempString, i, 0);
				}
			}
		} 

		catch (Exception e) {
			System.out.println("Grave erreur dans la mthode SetRules() "
					+ "de la classe RulesPanel !");
		}	
		
	}
	

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent ev) {
		Object sourcev = ev.getSource();
		Object line[] = new Object[nbr_col];
		
		String tempString;
		
		int NbrLine = tableau.getRowCount();

		System.out.println("Add or Delete Line pressed !");

		if (sourcev == But_AddLine) {
			dftable.addRow(line);
		}

		if (sourcev == But_DeleteLine && dftable.getRowCount() > 0) {
			dftable.removeRow(dftable.getRowCount() - 1);
		}
		
		if(sourcev == But_RemoveEmptyLines && dftable.getRowCount() > 0) {

			int i=0;
			// on scanne chaque ligne et on retire les lignes vides
			while(i < NbrLine) {
				tempString = (String)tableau.getValueAt(i, 0);
				
				if (tempString != null && (tempString.indexOf('\n')==0 || tempString.equals("") ||
						tempString.indexOf(' ')==0 || tempString.equals("  ")) ) 
				{
//					System.out.println("Retirer une ligne vide");
					dftable.removeRow(i);
					NbrLine = tableau.getRowCount();
					i=0;
				}
				else if (tempString == null) 
				{
					dftable.removeRow(i);
					NbrLine = tableau.getRowCount();
					i=0;
				}
				else {
					i++;	
				}				
			}
		}
		
		if (sourcev == But_DeleteAllLines && dftable.getRowCount() > 0) {
			while(dftable.getRowCount()>0){
				dftable.removeRow(dftable.getRowCount() - 1);
			}
		}

	}

}
