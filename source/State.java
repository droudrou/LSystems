//import java.io.*;

import javax.swing.*;
import java.awt.*;

/**
 * Cette classe permet de grer les tats d'un LSystem.
 * En effet, on peut avoir besoin suivant le systme de rgles d'enregistrer la
 * position courante, ou de revenir  la dernire position courante enregistre.
 * Cela suppose donc le recours  des objets de type tat que l'on empilera ou dpilera
 * selon les besoins.
 * <p>
 * Les quatre mthodes de la classe State associes  la construction graphique du LSystem sont :
 * <ul>
 * <li> AvanceDraw : dplace la tortue et dessine un trait 
 * <li> AvancePoint : dplace la tortue
 * <li> draw_line : dessine un trait
 * <li> Rotation : fait tourner la tortue d'une valeur gale  Angle
 * </ul>
 * <p>
 * Les quatre attibuts d'un tat sont :
 * <ul>
 * <li> Position : Position de la tortue 
 * <li> Orientation : Orientation de la tortue
 * <li> Angle : Angle d'une rotation lmentaire de la tortue
 * <li> Lenght : Longueur d'un dplacement lmentaire de la tortue
 * </ul>
 * 
 * @author Mathieu Drouin
 * @version 0.9
 */
public class State extends JPanel {

	private static final long serialVersionUID = 00001000L;

	private Point Position;

	/**
	 * Orientation en radians.
	 */
	private double Orientation;
	
	/**
	 * Angle en degrs.
	 */
	private double Angle;

	private float Lenght;
	
	private Color couleur;
	private int ColorIndex;
	
	private static Color[] tabColor = {Color.black, Color.darkGray, Color.gray, Color.lightGray, 
		Color.blue, Color.cyan, Color.green, Color.yellow, Color.orange, Color.red, 
		Color.magenta, Color.pink};
	
	/**
	 * Constructeur par dfaut
	 * 
	 */
	public State() {
		Position = new Point(0, 0);
		Orientation = 0;
		Angle = 30;
		Lenght = 100;
		couleur = Color.black;
		ColorIndex = 0;
		
		System.out.println("++ constructeur State()");
	}

	/**
	 * Constructeur par recopie
	 * 
	 * @param copie
	 */
	public State(State copie) {
		Position = new Point(copie.Position);
		Orientation = copie.Orientation;
		Angle = copie.Angle;
		Lenght = copie.Lenght;
		couleur = copie.couleur;
		
		// System.out.println("++ constructeur State() de recopie");
	}

	/**
	 * Constructeur complet version1
	 * 
	 * @param Position
	 * @param Angle
	 * @param Lenght
	 */
	public State(Point Position, float Angle, float Lenght) {
		this.Position = new Point(Position);
		this.Orientation = 0;
		this.Angle = Angle;
		this.Lenght = Lenght;
		this.couleur = Color.black;
		ColorIndex = 0;
		
		System.out
				.println("++ constructeur State( Point Position, float Orientation, float Angle, float Lenght )");
	}

	/**
	 * Constructeur complet version2
	 * 
	 * @param Position
	 * @param Orientation
	 * @param Angle
	 * @param Lenght
	 */
	public State(Point Position, float Angle, float Lenght, float InitialAngle ) {
		this.Position = new Point(Position);
		this.Orientation = -InitialAngle * (float)Math.PI / 180;
		this.Angle = Angle;
		this.Lenght = Lenght;
		this.couleur = Color.black;
		ColorIndex = 0;
		
		System.out
				.println("++ constructeur State( Point Position, float Orientation, float Angle, float Lenght )");
	}

	/**
	 * 
	 * @return Renvoie la position de l'tat
	 */
	public Point GetPosition() {
		return this.Position;
	}

	/**
	 * 
	 * @return Renvoie l'orientation de l'tat par rapport   la verticale
	 */
	public double GetOrientation() {
		return this.Orientation;
	}

	/**
	 * 
	 * @return Renvoie la longueur de trait
	 */
	public float GetLenght() {
		return this.Lenght;
	}

	/**
	 * Pour modifier la longueur de trait.
	 * 
	 * @param Lenght
	 */
	public void SetLength(float Lenght) {
		this.Lenght = Lenght;
	}

	/**
	 * 
	 * @return Renvoie l'angle de rotation lmentaire en degrs
	 */
	public double GetAngle() {
		return Angle;
	}

	/**
	 * Modification de l'angle de rotation lmentaire en degrs.
	 * 
	 * @param Angle
	 */
	public void SetAngle(float Angle) {
		this.Angle = Angle;
	}

	/**
	 * Cette mthode permet de dplacer la tortue et dessine le trait associ  ce dplacement, 
	 * on utilise pour cela les valeurs de Orientation et Lenght.
	 * 
	 * @param Screen Afin d'appeler la mthode drawLine de java on doit transmettre 
	 * un contexte graphique
	 */
	public void AvanceDraw(Graphics Screen) {
		float Tx, Ty;

		Tx = Lenght * (float) (-Math.sin(Orientation));
		Ty = Lenght * (float) (Math.cos(Orientation));

		/*
		 * System.out.println( "Orientation = " + Orientation ) ;
		 * System.out.println( "Lenght = " + Lenght ) ; System.out.println( "Tx = " +
		 * Tx ) ; System.out.println( "Ty = " + Ty ) ;
		 */

		draw_Line(Screen, Tx, Ty);

		// on translate la Position suivant l'orientation
		Position.set_x(Position.get_x() + Tx);
		Position.set_y(Position.get_y() + Ty);

		// System.out.println(" Avance point ! ") ;
	}

	/**
	 * Cette mthode permet de dplacer la tortue mais ne dessine rien, 
	 * on utilise pour cela les valeurs de Orientation et Lenght.
	 *  
	 */
	public void AvancePoint() {
		float Tx, Ty;

		Tx = Lenght * (float) (-Math.sin(Orientation));
		Ty = Lenght * (float) (Math.cos(Orientation));

		// on translate la Position suivant l'orientation
		Position.set_x(Position.get_x() + Tx);
		Position.set_y(Position.get_y() + Ty);
	}

	/**
	 * Cette mthode dessine un trait.
	 * On utilise comme origine du trait le point position.
	 * 
	 * @param Screen Le contexte graphique du panneau o le trait sera dessin
	 * @param Tx Tanslation suivant Ox
	 * @param Ty Tanslation suivant Oy
	 */
	public void draw_Line(Graphics Screen, float Tx, float Ty) {

		Screen.setColor(couleur);		
		
		Screen.drawLine((int) (Position.get_x()), (int) (Position.get_y()),
				(int) (Position.get_x() + Tx), (int) (Position.get_y() + Ty));

		// System.out.println("Une ligne vient d'tre trace") ;
	}
	
	/**
	 * Cette mthode permet de modifier l'orientation de la tortue, en ajoutant 
	 *  Orientation un multiple unitaire de l'attribut Angle.
	 * <p>
	 * On effectue une conversion de la valeur de l'attribut Angle
	 * pour dterminer Orientation en radians.
	 * 
	 * @param signe +1 pour tourner dans le sens trigo, et -1 dans le 
	 * sens des aiguilles d'une montre
	 */
	public void Rotation(int signe) {
		Orientation += signe / Math.abs((double) signe) * Angle
				* Math.PI / 180;

		// System.out.println(" Rotation ! ") ;
	}
	
	
	public void IncColor() {
		ColorIndex = (ColorIndex+1)%tabColor.length;
		couleur = tabColor[ColorIndex];
	}
	
	public void DecColor() {
		ColorIndex = (ColorIndex-1)%tabColor.length;
		couleur = tabColor[ColorIndex];
	}	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String chaine = null;

		chaine = Position.toString();
		chaine += "Orientation : \n";
		chaine += "alpha (rad) = " + Orientation;

		return (chaine);
	}

}
